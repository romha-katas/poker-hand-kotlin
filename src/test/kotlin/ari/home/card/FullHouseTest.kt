package ari.home.card

import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class FullHouseTest {

    @Test
    fun `should find a full house`() {
        "♢2 ♢J ♤2 ♡2 ♧J" `should expose a` Rule.FullHouse("♢2 ♢J ♤2 ♡2 ♧J")
    }

    @Test
    fun `A flush should win against weaker hand`() {
        "♢2 ♢J ♤2 ♡2 ♧J" `should win against` "♧2 ♧J ♧3 ♧5 ♧Q"
    }

    @Test
    fun `A flush should win by its higher card`() {
        "♢7 ♢J ♤7 ♡7 ♧J" `should win against` "♢2 ♢J ♤2 ♡2 ♧J"
    }
}
