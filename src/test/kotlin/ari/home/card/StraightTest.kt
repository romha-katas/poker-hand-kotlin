package ari.home.card

import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class StraightTest {

    @Test
    fun `should find a straight`() {
        "♢2 ♢3 ♤4 ♡5 ♧6" `should expose a` Rule.Straight("♢2 ♢3 ♤4 ♡5 ♧6")
    }

    @Test
    fun `A straight should win against weaker hand`() {
        "♢2 ♢3 ♤4 ♡5 ♧6" `should win against` "♢K ♧K ♧3 ♡K ♧9"
    }

    @Test
    fun `the highest value should win`() {
        "♢10 ♢J ♤Q ♡K ♧A" `should win against` "♢7 ♢8 ♤9 ♡10 ♧J"
    }

}
