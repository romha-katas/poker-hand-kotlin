package ari.home.card

import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class FlushTest {

    @Test
    fun `should find a flush`() {
        "♧2 ♧J ♧3 ♧5 ♧K" `should expose a` Rule.Flush("♧K ♧J ♧5 ♧3 ♧2")
    }

    @Test
    fun `A flush should win against weaker hand`() {
        "♧2 ♧J ♧3 ♧5 ♧Q" `should win against` "♢2 ♢3 ♤4 ♡5 ♧6"
    }

    @Test
    fun `A flush should win by its higher card`() {
        "♧2 ♧J ♧3 ♧5 ♧K" `should win against` "♢2 ♢Q ♢3 ♢4 ♢9"
    }

    @Test
    fun `should be even`() {
        "♧2 ♧J ♧3 ♧5 ♧K" `should be even` "♢2 ♢J ♢3 ♢5 ♢K"
    }
}
