package ari.home.card
import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class PairTest {

    @Test
    fun `should find a pair`() {
        "♢2 ♢J ♤3 ♡2 ♧K" `should expose a` Rule.Pair("♢2 ♡2")
    }

    @Test
    fun `should win against weaker hand`() {
        "♢J ♢K ♤J ♡4 ♧2" `should win against` "♢2 ♧J ♧3 ♡2 ♧Q"
    }

    @Test
    fun `hand with the highest pair should win`() {
        "♢J ♢K ♤J ♡4 ♧2" `should win against` "♢3 ♧J ♧3 ♡6 ♧Q"
    }

    @Test
    fun `hand with the highest card should win when pair are the same`() {
        "♢J ♢K ♤J ♡4 ♧2" `should win against` "♢2 ♧J ♧3 ♡J ♧Q"
    }
}
