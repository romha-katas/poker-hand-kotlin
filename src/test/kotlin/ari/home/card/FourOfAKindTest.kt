package ari.home.card

import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class FourOfAKindTest {

    @Test
    fun `should find a four of a kind`() {
        "♢2 ♢J ♤2 ♡2 ♧2" `should expose a` Rule.FourOfAKind("♢2 ♤2 ♡2 ♧2")
    }

    @Test
    fun `should win against weaker hand`() {
        "♢2 ♢J ♤2 ♡2 ♧2" `should win against` "♢2 ♢J ♤2 ♡2 ♧J"
    }

    @Test
    fun `A flush should win by its higher card`() {
        "♢7 ♢J ♤7 ♡7 ♧7" `should win against` "♢2 ♢J ♤2 ♡2 ♧2"
    }
}
