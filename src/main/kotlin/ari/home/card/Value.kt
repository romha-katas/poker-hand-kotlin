package ari.home.card

enum class Value {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE;

    operator fun minus(rValue: Value): Int {
        return (this.ordinal + 1) - (rValue.ordinal + 1)
    }
}
