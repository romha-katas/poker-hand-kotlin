package ari.home.card

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CardOrderTest {

    @Test
    fun `higher card should beat lower card`() {
        "♡A"  beats "♢K"
        "♡K"  beats "♢Q"
        "♡Q"  beats "♢J"
        "♡J"  beats "♢10"
        "♡10" beats "♢9"
        "♡9"  beats "♢8"
        "♡8"  beats "♢7"
        "♡7"  beats "♢6"
        "♡6"  beats "♢5"
        "♡5"  beats "♢4"
        "♡4"  beats "♢3"
        "♡3"  beats "♢2"
    }

    @Test
    fun `two cards with same value should be even`() {
        "♡A"  even "♢A"
    }

    private infix fun String.beats(other: String) {
        Assertions.assertEquals(1, this.toCard() compareTo other.toCard())
    }

    private infix fun String.even(other: String) {
        Assertions.assertEquals(0, this.toCard() compareTo other.toCard())
    }
}


