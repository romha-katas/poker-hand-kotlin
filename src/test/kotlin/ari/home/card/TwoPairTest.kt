package ari.home.card
import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class TwoPairTest {

    @Test
    fun `should find a two pairs`() {
        "♢2 ♢J ♤3 ♡2 ♧J" `should expose a` Rule.TwoPairs("♢J ♧J ♢2 ♡2")
    }

    @Test
    fun `should win against weaker hand`() {
        "♢J ♢K ♤J ♡4 ♧2" `should win against` "♢2 ♧J ♧3 ♡2 ♧Q"
    }

    @Test
    fun `hand with the highest pair should win `() {
        "♢J ♢4 ♤J ♡4 ♧2" `should win against` "♢3 ♧5 ♧3 ♡5 ♧Q"
    }

    @Test
    fun `hand with the second highest pair should win `() {
        "♢4 ♤J ♡4 ♧2 ♢J" `should win against` "♧J ♢3 ♧3 ♡J ♧Q"
    }

    @Test
    fun `hand with the highest card should win when pairs are the same`() {
        "♢J ♢K ♤J ♡4 ♧2" `should win against` "♢2 ♧J ♧3 ♡J ♧Q"
    }
}
