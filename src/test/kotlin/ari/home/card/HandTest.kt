package ari.home.card
import org.junit.jupiter.api.Test

class HandTest {

    @Test
    fun `hand with the highest card should win when no combination`() {
        "♢J ♢K ♤J ♡4 ♧2" `should win against` "♢2 ♧J ♧3 ♡6 ♧Q"
    }

    @Test
    fun `hand with the second highest card should win when no combination`() {
        "♢J ♢K ♤8 ♡4 ♧2" `should win against` "♢2 ♧K ♧3 ♡6 ♧9"
    }
}
