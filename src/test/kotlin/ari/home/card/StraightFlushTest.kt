package ari.home.card

import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class StraightFlushTest {

    @Test
    fun `should find a straight flush`() {
        "♢2 ♢3 ♢4 ♢5 ♢6" `should expose a` Rule.StraightFlush("♢2 ♢3 ♢4 ♢5 ♢6")
    }

    @Test
    fun `should win against weaker hand`() {
        "♢2 ♢3 ♢4 ♢5 ♢6" `should win against` "♢2 ♢J ♤2 ♡2 ♧2"
    }

    @Test
    fun `A flush should win by its higher rank`() {
        "♢10 ♢J ♢Q ♢K ♢A" `should win against` "♢7 ♢8 ♢9 ♢10 ♢J"
    }
}
