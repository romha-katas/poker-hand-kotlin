package ari.home.card

import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class ThreeOfAKind {

    @Test
    fun `should find a three of a kind`() {
        "♢2 ♢Q ♤2 ♡2 ♧J" `should expose a` Rule.ThreeOfAKind("♢2 ♤2 ♡2")
    }

    @Test
    fun `should win against weaker hand`() {
        "♢J ♢K ♤J ♡J ♧2" `should win against` "♢3 ♧J ♧3 ♡5 ♧5"
    }

    @Test
    fun `the highest value should win`() {
        "♢J ♢K ♤J ♡J ♧2" `should win against` "♢3 ♧J ♧3 ♡3 ♧Q"
    }
}
