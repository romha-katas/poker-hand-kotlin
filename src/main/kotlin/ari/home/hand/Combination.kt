package ari.home.hand

import ari.home.card.Card

data class Combination(val rule: Rule, val cards: List<Card>): Comparable<Combination> {
    override fun compareTo(other: Combination): Int {
        val ruleComparison = this.rule.compareTo(other.rule)

        if (ruleComparison == 0) {
            cards.zip(other.cards) { left, right ->
                if (left.compareTo(right) != 0)
                    return left.compareTo(right)
            }
            return 0
        }
        return ruleComparison
    }



}
