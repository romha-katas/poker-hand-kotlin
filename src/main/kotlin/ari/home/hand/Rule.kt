package ari.home.hand

import ari.home.card.Card

enum class Rule {
    HighCard {
        override fun matches(hand: Hand): Combination {
            val cards = hand.draw(5).sortedDescending()
            return Combination(this, cards)
        }
    },
    Pair {
        override fun matches(hand: Hand): Combination? {
            val groups = hand.groupOfAtLeast(2)
            return if (groups.isNotEmpty()) {
                Combination(this, groups.first() + hand.without(groups.first()))
            } else {
                null
            }
        }
    },
    TwoPairs {
        override fun matches(hand: Hand): Combination? {
            val groups = hand.groupOfAtLeast(2)
            return if (groups.count() == 2) {
                Combination(this, groups.first() + groups.drop(1).first() + hand.without(groups.subList(0, 2).flatten()))
            } else {
                null
            }
        }
    },
    ThreeOfAKind {
        override fun matches(hand: Hand): Combination? {
            val groups = hand.groupOfAtLeast(3)
            return if (groups.isNotEmpty()) {
                Combination(this, groups.first())
            } else {
                null
            }
        }
    },
    Straight {
        override fun matches(hand: Hand): Combination? {
            val values = hand.values.distinct().sorted()
            return if (values.last() - values.first() == 4)
                Combination(this, hand.draw(5))
                else null
        }
    },
    Flush {
        override fun matches(hand: Hand): Combination? {
            return if (hand.map{it.suit }.distinct().count() == 1)
                Combination(this, hand.draw(5).sortedDescending())
            else null
        }
    },
    FullHouse {
        override fun matches(hand: Hand): Combination? {
            val groups = hand.groupOfAtLeast(2).map { it.count() }
            return if (groups.contains(2) && groups.contains(3))
                return Combination(this, hand.draw(5))
            else null
        }
    },

    FourOfAKind {
        override fun matches(hand: Hand): Combination? {
            val groups = hand.groupOfAtLeast(4)
            return if (groups.count() == 1)
                return Combination(this, groups.first())
            else null
        }
    },
    StraightFlush {
        override fun matches(hand: Hand): Combination? {
            val values = hand.values.distinct().sorted()
            val suits = hand.map{it.suit }.distinct().count()
            return if (suits == 1 && values.last() - values.first() == 4)
                Combination(this, hand.draw(5))
            else null
        }
    }
    ;

    abstract fun matches(hand: Hand): Combination?
}

private fun Hand.without(removed: List<Card>): List<Card> {
    return draw(5).filter { !removed.contains(it) }
}


private fun Hand.groupOfAtLeast(count: Int): List<List<Card>> {
    return this.groupingBy { it.value }.eachCount().filter { it.value >= count }.keys.sortedDescending()
        .map { value -> this.filter { it.value == value } }
}
