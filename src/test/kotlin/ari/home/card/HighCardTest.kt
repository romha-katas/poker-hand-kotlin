package ari.home.card

import ari.home.hand.Rule
import org.junit.jupiter.api.Test

class HighCardTest {

    @Test
    fun `should find the highest card`() {
        "♢A ♢J ♤3 ♡2 ♧K" `should expose a` Rule.HighCard("♢A")
    }

    @Test
    fun `should win by its higher rank`() {
        "♢A ♢J ♤3 ♡2 ♧K" `should win against` "♢5 ♢J ♤3 ♡2 ♧K"
    }

    @Test
    fun `should win by its second higher rank`() {
        "♢A ♢J ♤3 ♡2 ♧K" `should win against` "♢A ♢J ♤3 ♡2 ♧Q"
    }
}




/*
2 3 4 5 6 7 8 9 10 J Q K A
♡ ♢ ♧ ♤
 */
