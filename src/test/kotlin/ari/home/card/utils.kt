package ari.home.card

import ari.home.hand.Combination
import ari.home.hand.Hand
import ari.home.hand.Rule
import org.assertj.core.api.Assertions

fun String.toHand(): Hand {

    return Hand(*this.toCards().toTypedArray())
}

fun String.toCards(): List<Card> {
    return this.split(" ").map { it.toCard() }
}

fun String.toCard(): Card {
    val suit = this.first().asSuit()
    val value = this.drop(1).asValue()
    return Card(value, suit)
}

private fun String.asValue(): Value {
    return when (this) {
        "2" -> Value.TWO
        "3" -> Value.THREE
        "4" -> Value.FOUR
        "5" -> Value.FIVE
        "6" -> Value.SIX
        "7" -> Value.SEVEN
        "8" -> Value.EIGHT
        "9" -> Value.NINE
        "10" -> Value.TEN
        "J" -> Value.JACK
        "Q" -> Value.QUEEN
        "K" -> Value.KING
        "A" -> Value.ACE
        else -> throw IllegalArgumentException("$this is not a valid value")
    }
}

private fun Char.asSuit(): Suit {
    return when (this) {
        '♡' -> Suit.HEART
        '♢' -> Suit.DIAMOND
        '♧' -> Suit.CLUBS
        '♤' -> Suit.SPADE
        else -> throw IllegalArgumentException("$this is not a valid suit")
    }
}


infix fun String.`should win against`(other: String) {
    val left = this.toHand()
    val right = other.toHand()

    Assertions.assertThat(left.compareTo(right)).isGreaterThan(0)
}

infix fun String.`should be even`(other: String) {
    val left = this.toHand()
    val right = other.toHand()

    Assertions.assertThat(left.compareTo(right)).isEqualTo(0)
}


operator fun Rule.invoke(cards: String) = Combination(this, cards.toCards())

infix fun String.`should expose a`(expected: Combination) {
    val hand = this.toHand()
    val result = hand.bestCombination()

    Assertions.assertThat(result).isEqualByComparingTo(expected)
}
