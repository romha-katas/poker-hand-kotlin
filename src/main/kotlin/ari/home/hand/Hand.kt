package ari.home.hand

import ari.home.card.Card
import ari.home.card.Value

class Hand(vararg card: Card): Comparable<Hand> {
    private val cards = card.toSet()

    val values: List<Value>
            get() = cards.map { it.value }

    init {
        require(this.cards.size == 5) { "A hand must contain 5 cards" }
    }

    fun draw(count: Int): List<Card> {
        return cards.take(count)
    }

    fun <T> groupingBy(keySelector: (Card) -> T): Grouping<Card, T> = cards.groupingBy(keySelector)

    fun filter(predicate: (Card) -> Boolean) = cards.filter(predicate)
    fun <T> map(mapper: (Card) -> T): List<T> = this.cards.map(mapper)

    fun bestCombination(): Combination {
        val rules = Rule.values()
        return rules.mapNotNull { it.matches(this) }.last()
    }

    override fun compareTo(other: Hand): Int {
        return this.bestCombination().compareTo(other.bestCombination())
    }

}

