package ari.home.card

enum class Suit {
    HEART,
    DIAMOND,
    CLUBS,
    SPADE
}
